FROM node:8.12.0-alpine as builder

WORKDIR /src
COPY package.json .
COPY package-lock.json .
RUN npm install

COPY . .
RUN npm run build

FROM nginx:1.15.3-alpine as runtime
WORKDIR /usr/share/nginx/html
COPY --from=builder /src/build .
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf